import _ from "lodash";

export function paginate(items, pageNumber, pageSize) {
  const startIndex = (pageNumber - 1) * pageSize; // indice di partenza dell'array
  /* Convertiamo l'item array in lodash object(con _(item) per scrivere una sintassi più coincisa)
        e poi si riconverte in array con il metodo value() */
  return _(items)
    .slice(startIndex)
    .take(pageSize)
    .value();
}
