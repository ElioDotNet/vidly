import React from "react";
import auth from "../../services/authService";

import { Route, Redirect } from "react-router-dom";

// Creazione di un component Route Protetto
const ProtectedRoute = ({ path, component: Component, render, ...rest }) => { // React vuole il component in maiuscolo
    return (
        <Route
            // ...rest: si prendono tutte le restanti proprietà
            {...rest}
            render={props => {
                // Si viene reindirizzati da dove si è partiti
                const currentUser = auth.getCurrentUser();
                if (!currentUser)
                    return <Redirect to={{
                        pathname: '/login',
                        state: { from: props.location }
                }} />;
                return Component ? <Component {...props} /> : render(props);
            }}
        />
    );
};

export default ProtectedRoute;