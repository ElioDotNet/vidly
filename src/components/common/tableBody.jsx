import React, { Component } from "react";
import _ from "lodash";

// Creiamo questa classe per potere utilizzare la tabella Body in qualsisasi contesto
class TableBody extends Component {
  renderCell = (item, column) => {
    if (column.content) return column.content(item); // se contiene il content esegue la funzione

    return _.get(item, column.path); // diversamente ritorna l'item e il path
  };

  createKey = (item, column) => {
    // Creazione chiave univoca per il td della table
    return item._id + (column.path || column.key);
  };

  render() {
    const { data, columns } = this.props;

    return (
      <tbody>
        {data.map(item => (
          <tr key={item._id}>
            {columns.map(column => (
              <td key={this.createKey(item, column)}>
                {this.renderCell(item, column)}
              </td>
            ))}
          </tr>
        ))}
      </tbody>
    );
  }
}

export default TableBody;
