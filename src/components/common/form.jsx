import React, { Component } from "react";

import Joi from "joi-browser";

import Input from "./input";
import Select from "./select";

class Form extends Component {
  state = {
    data: {},
    errors: {}
  };

  // Valida l'intero form
  validate = () => {
    const options = { abortEarly: false }; // si controllamo tutti i campi non solo il primo
    const { error } = Joi.validate(this.state.data, this.schema, options);
    if (!error) {
      // l'oggetto error è vuoto: non ci sono errori
      return null;
    }
    // Con Joi viene popolata un array di errori
    const errors = {};

    for (let item of error.details) {
      errors[item.path[0]] = item.message;
    }

    return errors;
  };

  // Valida le singole proprietà
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value }; // mettendo [name] crea una key dinamica basata sul name
    const schema = { [name]: this.schema[name] }; // utilizza lo schema solo della key name
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };

  handleSubmit = e => {
    e.preventDefault(); // previene il reload della pagina che si ha con il submit del form

    const errors = this.validate();
    this.setState({ errors: errors || {} }); // se errore è null restituisco oggetto vuoto
    if (errors) return;

    this.doSubmit();
  };

  // Mettiamo un evento generico per tutti i campi
  handleChange = ({ currentTarget: input }) => {
    const errors = { ...this.state.errors };
    const errorMessage = this.validateProperty(input);
    if (errorMessage) {
      errors[input.name] = errorMessage;
    } else {
      delete errors[input.name]; // cancella la proprietà
    }
    const data = { ...this.state.data };
    data[input.name] = input.value;
    this.setState({ data, errors });
  };

  renderButton(label) {
    // this.validate() ritorna null(false) oppure un oggetto(true)
    return (
      <button disabled={this.validate()} className="btn btn-primary">
        {label}
      </button>
    );
  }

  renderSelect(name, label, options) {
    const { data, errors } = this.state;

    return (
      <Select
        name={name}
        value={data[name]}
        label={label}
        options={options}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  }

  renderInput(name, label, type = "text") {
    const { data, errors } = this.state;
    return (
      <Input
        type={type}
        name={name}
        value={data[name]}
        label={label}
        onChange={this.handleChange}
        error={errors[name]}
      />
    );
  }
}

export default Form;
