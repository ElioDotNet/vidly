import React from "react";

const ListGroup = props => {
    const {items, valueProperty, textProperty, onItemSelect, selectedItem} = props;
    let activeClass = "list-group-item ";

    return (
        <ul className="list-group">
            {items.map(item => (
                <li onClick={onItemSelect(item)} key={item[valueProperty]}  // passa il genere al movies tramite onItemSelect
                    className={item === selectedItem ? activeClass + "active": activeClass}>
                    {item[textProperty]}
                </li>
            ))}
        </ul>
    );
};
// Definiamo delle proprietà di default relative all'id e al testo da usare nella lista
ListGroup.defaultProps = {
    textProperty: "name",
    valueProperty: "_id"
};

export default ListGroup;
