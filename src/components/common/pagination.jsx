/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react";

import PropTypes from "prop-types";
import _ from "lodash";

const Pagination = props => {
  const { itemsCount, pageSize, currentPage, onPageChange } = props;
  const pageCount = Math.ceil(itemsCount / pageSize);
  if (pageCount === 1) return null; // se c'è solo una pagina non visualizza nessuna pagina
  const pages = _.range(1, pageCount + 1); // aggiungendo + 1 si ha anche l'ultima pagina

  return (
    <nav>
      <ul className="pagination">
        {pages.map(page => (
          <li
            key={page}
            className={page === currentPage ? "page-item active" : "page-item"}
          >
            <a href="#" className="page-link" onClick={onPageChange(page)}>
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};
// Definiamo le proprietà delle variabili usate nel component
Pagination.propType = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
};

export default Pagination;
