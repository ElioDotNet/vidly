import React, { Component } from "react";

// Creiamo questa classe per potere utilizzare la tabella Header in qualsisasi contesto
class TableHeader extends Component {
  raiseSort = path => () => {
    const sortColumn = { ...this.props.sortColumn };
    // Se clicco due volte sull'ordinamento cambio l'ordine
    if (sortColumn.path === path)
      sortColumn.order = sortColumn.order === "asc" ? "desc" : "asc";
    else {
      // Diversamente imposto il nuovo ordinamento
      sortColumn.order = "asc";
      sortColumn.path = path;
    }
    // Genera Evento di ordinamento
    this.props.onSort(sortColumn);
  };

  renderSortIcon = column => {
    const { sortColumn } = this.props;
    /* Se la colonna di ordinamento è diversa dalla colonna non metto nessuna icona
     altrimenti inserisco una icona di ordinamento in su o in giù */
    if (column.path !== sortColumn.path) return null;
    if (sortColumn.order === "asc") return <i className="fa fa-sort-asc" />;
    return <i className="fa fa-sort-desc" />;
  };

  render() {
    return (
      <thead>
        <tr>
          {this.props.columns.map(column => (
            <th className="clikable"
              key={column.path || column.key}
              onClick={this.raiseSort(column.path)}
            >
              {column.label} {this.renderSortIcon(column)}
            </th>
          ))}
        </tr>
      </thead>
    );
  }
}

export default TableHeader;
