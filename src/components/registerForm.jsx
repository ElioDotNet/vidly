import React from "react";
import Form from "./common/form";
import Joi from "joi-browser";

import * as userService from "../services/userService";
import auth from "../services/authService";

class RegisterForm extends Form {
  // Inizializziamo i valori del form: null o undefined non sono ammessi
  state = {
    data: {
      username: "",
      password: "",
      name: ""
    },
    errors: {}
  };

  // Definiamo lo schema per la validazione dei dati
  schema = {
    username: Joi.string()
      .required()
      .email()
      .label("Username"), // per avere nel messaggio la lettera maiscola
    password: Joi.string()
      .required()
      .min(5)
      .label("Password"),
    name: Joi.string()
      .required()
      .label("Name")
  };

  doSubmit = async () => {
    try {
      // Inseriamo lo user
      const response = await userService.register(this.state.data);
      // Dalla response abbiamo impostato l'header per ritornare il token
      auth.loginWithJwt(response.headers["x-auth-token"]);
      window.location = '/'; // viene riletta la pagina e lette le info per lo user loggato(app.jsx)
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = {...this.state.errors};
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    return (
      <div>
        <h1>Register</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("username", "Username")}
          {this.renderInput("password", "Password", "password")}
          {this.renderInput("name", "Name")}
          {this.renderButton("Register")}
        </form>
      </div>
    );
  }
}

export default RegisterForm;
