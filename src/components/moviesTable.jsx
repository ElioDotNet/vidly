import React, { Component } from "react";
import { Link } from "react-router-dom";

import auth from "../services/authService";

import Like from "./common/like";
import Table from "./common/table";

// Creiamo questa classe per poter rendere generico l'ordinamento
class MoviesTable extends Component {
  columns = [
    {
      path: "title",
      label: "Title",
      content: movie => <Link to={`/movies/${movie._id}`}>{movie.title}</Link>
    },
    { path: "genre.name", label: "Genre" },
    { path: "numberInStock", label: "Stock" },
    { path: "dailyRentalRate", label: "Rate" },
    // Colonne per il like e il bottone di delete
    {
      key: "like",
      // usiamo una funzione che restituisce un component Like come qualsiasi altro tag html
      content: movie => (
        <Like liked={movie.like} onClick={this.props.onLike(movie)} />
      )
    }
  ];

  deleteColumn = {
      key: "delete",
      content: movie => (
        <button
          onClick={this.props.onDelete(movie)}
          className="btn btn-danger btn-sm"
        >
          Delete
        </button>
      )
    }

  constructor() {
    super();
    const user = auth.getCurrentUser();
    if (user && user.isAdmin)
      this.columns.push(this.deleteColumn);
  }

render() {
  const { movies, onSort, sortColumn } = this.props;
  return (
    <Table
      columns={this.columns}
      data={movies}
      sortColumn={sortColumn}
      onSort={onSort}
    />
  );
}
}

export default MoviesTable;
