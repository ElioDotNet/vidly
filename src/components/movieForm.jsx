import React from "react";

import Joi from "joi-browser";

import Form from "./common/form";

// import { getGenres } from "../services/genreService";
// import { getMovie, saveMovie } from "../services/movieService";
import { getGenres } from "../services/fakeGenreService";
import { getMovie, saveMovie } from "../services/fakeMovieService";

class MovieForm extends Form {
  state = {
    data: {
      title: "",
      genreId: "",
      numberInStock: "",
      dailyRentalRate: ""
    },
    genres: [],
    errors: {}
  };

  // Definiamo lo schema per la validazione dei dati
  schema = {
    _id: Joi.string(),
    title: Joi.string()
      .required()
      .label("Title"),
    genreId: Joi.string()
      .required()
      .label("Genre"),
    numberInStock: Joi.number()
      .required()
      .min(0)
      .max(100)
      .label("Number in Stock"),
    dailyRentalRate: Joi.number()
      .required()
      .min(0)
      .max(10)
      .label("Daily Rental Rate")
  };

  populateGenres() {
    const genres = getGenres();
    this.setState({ genres });
  }

  async populateMovie() {
    const genres = getGenres();
    this.setState({ genres });

    const movieId = this.props.match.params.id;
    if (movieId === "new") return; // se è un nuovo film non occore popolare il form

    const movie = getMovie(movieId);
    // Viene usato replace per poter tornare indietro con il pulsante back
    if (!movie) return this.props.history.replace("/not-found");

    this.setState({ data: this.mapToViewModel(movie) });
  }

  // *** GESTIONE CON SERVICE REALI ***
  // async populateGenres() {
  //   const { data: genres} = await getGenres();
  //   this.setState({ genres });
  // }

  // async populateMovie() {
  //   try {
  //     const movieId = this.props.match.params.id;
  //     if (movieId === "new") {
  //       // se è un nuovo film non occore popolare il form
  //       return;
  //     }
  //     const { data: movie } = await getMovie(movieId);
  //     this.setState({ data: this.mapToViewModel(movie) });
  //   }
  //   catch (ex) {
  //     if (ex.response && ex.response.status === 404) {
  //       // viene usato replace per poter tornare indietro con il pulsante back
  //         this.props.history.replace("/not-found");
  //     }
  //   }
  // }

  // async componentDidMount() {
  //   await this.populateGenres();
  //   await this.populateMovie();
  // }

  componentDidMount() {
    this.populateGenres();
    this.populateMovie();
  }

  mapToViewModel(movie) {
    return {
      _id: movie._id,
      title: movie.title,
      genreId: movie.genre._id,
      numberInStock: movie.numberInStock,
      dailyRentalRate: movie.dailyRentalRate
    };
  }

  // *** GESTIONE CON SERVICE REALI ***
  // doSubmit = async () => {
  //   await saveMovie(this.state.data);
  //   this.props.history.push("/movies");
  // };

  doSubmit = () => {
    saveMovie(this.state.data);
    this.props.history.push("/movies");
  };

  render() {
    return (
      <div>
        <h1>MoviesForm</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput("title", "Title")}
          {this.renderSelect("genreId", "Genre", this.state.genres)}
          {this.renderInput("numberInStock", "Number in Stock", "number")}
          {this.renderInput("dailyRentalRate", "Rate")}
          {this.renderButton("Save")}
        </form>
      </div>
    );
  }
}

export default MovieForm;