import React from "react";
import { Redirect } from "react-router-dom";
import Form from "./common/form";
import Joi from "joi-browser";
import auth from "../services/authService";

class LoginForm extends Form {
  // Inizializziamo i valori del form: null o undefined non sono ammessi
  state = {
    data: {
      username: "",
      password: ""
    },
    errors: {}
  };

  // Definiamo lo schema per la validazione dei dati
  schema = {
    username: Joi.string()
      .required()
      .label("Username"), // per avere nel messaggio la lettera maiscola
    password: Joi.string()
      .required()
      .label("Password")
  };

  doSubmit = async () => {
    try {
      const { data } = this.state;
      await auth.login(data.username, data.password);
      // Vengono presi i dati impostati in protectedRoute
      const { state } = this.props.location;
      window.location = state ? state.from.pathname: '/'; // se non è impostato lo stato si va alla Home
    } catch (ex) {
      if (ex.response && ex.response.status === 400) {
        const errors = {...this.state.errors};
        errors.username = ex.response.data;
        this.setState({ errors });
      }
    }
  };

  render() {
    if (auth.getCurrentUser()) return <Redirect to="/" />;
    return (
      <div>
        <h1>Login</h1>
        <form onSubmit={this.handleSubmit}>
          {this.renderInput('username','Username')}
          {this.renderInput('password','Password', 'password')}
          {this.renderButton('Login')}
        </form>
      </div>
    );
  }
}

export default LoginForm;
