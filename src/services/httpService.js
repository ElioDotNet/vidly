// VENGONO GESTITI TUTTI I METODI DI AXIOS: SI PUO' CAMBIARE LIBRERIA SENZA INFLUENZARE LA APP
import axios from "axios";
import logger from  "./logService";
import { toast } from "react-toastify";

/* Viene eseguita prima del catch
  nella use si accettano due parametri success e error, non gestendo la prima si passa null */
  axios.interceptors.response.use(null, error => {
    const expectedError =
      error.response &&
      error.response.status >= 400 &&
      error.response.status < 500;
    // Unexpected Error
    if (!expectedError) {
      logger.log(error);
      toast.error("An unexpected error occured.");
    }

    return Promise.reject(error);
  });

  function setJwt(jwt) {
    // Configurazione di default di Axios, per aggiungere un header
    axios.defaults.headers.common['x-auth-token'] = jwt;
  }

  export default {
      get: axios.get,
      post: axios.post,
      put: axios.put,
      delete: axios.delete,
      setJwt
  }