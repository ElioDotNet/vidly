import http from "./httpService";
import { apiUrl } from "../config.json";

import jwtDecode from "jwt-decode";

const apiEndPoint = apiUrl + '/auth';
const tokenKey = "token";

// Imposto qui il token anzichè nel httpservice, per evitare il Bi-directional Dependencies
http.setJwt(getJwt());

export async function login(email, password) {
    const { data: jwt} = await http.post(apiEndPoint, { email, password });
    localStorage.setItem(tokenKey, jwt);
}

export function loginWithJwt(jwt) {
    localStorage.setItem(tokenKey, jwt);
}

export function logout() {
    localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
    try {
        const jwt = localStorage.getItem(tokenKey);
        return jwtDecode(jwt);
      } catch (ex) {
          return null // nessun utente
      } // utente anonimo nessun token
}

export function getJwt() {
    return localStorage.getItem(tokenKey);
}

export default {
    login,
    loginWithJwt,
    logout,
    getCurrentUser
};