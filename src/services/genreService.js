import htpp from "./httpService";
import { apiUrl } from "../config.json";

export function getGenres() {
  return htpp.get(apiUrl + '/genres');
}